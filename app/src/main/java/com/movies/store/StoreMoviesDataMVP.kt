package com.movies.store

import com.movies.store.model.database.Movie

interface StoreMoviesDataMVP {
    interface View{
        fun showSavedMovie()
        fun showErrorTryingToSave()

        fun showMoviesStored(movies : List<Movie>)
    }

    interface Presenter{
        fun setView(view : StoreMoviesDataMVP.View)
        fun storeMovie(movie: Movie?)
        fun showInsetedElement()
        fun showInsertingError()

        fun getMoviesStored()
        fun showMoviesStored(movies : List<Movie>)
    }

    interface Model{
        fun setPresenter(presenter: Presenter)
        fun storeMovie(movie: Movie)

        fun getMoviesStored()
    }
}