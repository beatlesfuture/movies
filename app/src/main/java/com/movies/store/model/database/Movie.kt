package com.movies.store.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movie(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "movieTitle") var movieTitle : String,
    @ColumnInfo(name = "imagePath") var imgagePath : String,
    @ColumnInfo(name = "overview") var overview :String,
    @ColumnInfo(name = "language") var language : String )
