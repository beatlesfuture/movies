package com.movies.store.model

import android.content.Context
import com.movies.store.StoreMoviesDataMVP
import com.movies.store.model.database.AppDatabase
import com.movies.store.model.database.Movie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StoreModelImp(context : Context) : StoreMoviesDataMVP.Model {

    init {
        DaggerDatabaseComponent.builder()
            .databaseModule(DatabaseModule(context))
            .build()
            .inject(this)
    }

    @Inject
    lateinit var db : AppDatabase

    private var presenter : StoreMoviesDataMVP.Presenter? = null

    override fun setPresenter(presenter: StoreMoviesDataMVP.Presenter) {
        this.presenter = presenter
    }
    override fun storeMovie(movie: Movie) {
        CoroutineScope(Dispatchers.IO).launch {
            val movieDao = db.movieDao()

            val res = movieDao.insertMovie(movie)
            withContext(Dispatchers.Main) {
                if (res?.isNotEmpty() == true) {
                    presenter?.showInsetedElement()
                } else {
                    presenter?.showInsertingError()
                }
            }
        }
    }

    override fun getMoviesStored() {
        CoroutineScope(Dispatchers.IO).launch {
            val movieDao = db.movieDao()

            val res = movieDao.getMovies()
            withContext(Dispatchers.Main) {
                if (res?.isNotEmpty() == true) {
                    presenter?.showMoviesStored(res)
                }
            }
        }
    }
}