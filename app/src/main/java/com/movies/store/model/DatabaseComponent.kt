package com.movies.store.model

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf( DatabaseModule::class))
interface DatabaseComponent {
    fun inject(modelImp : StoreModelImp)
}