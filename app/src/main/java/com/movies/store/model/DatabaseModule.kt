package com.movies.store.model

import android.content.Context
import androidx.room.Room
import com.movies.store.model.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(var context: Context) {
    @Singleton
    @Provides
    fun proviesDatabase() : AppDatabase{
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "Movies"
        ).build()
    }


}