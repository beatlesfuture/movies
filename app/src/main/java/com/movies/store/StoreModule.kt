package com.movies.store

import android.content.Context
import com.movies.store.model.StoreModelImp
import com.movies.store.presenter.StorePresenterImp
import dagger.Module
import dagger.Provides

@Module
class StoreModule(var context : Context) {

    @Provides
    fun providesStorePresenter(model: StoreMoviesDataMVP.Model) : StoreMoviesDataMVP.Presenter{
        return StorePresenterImp(model)
    }
    @Provides
    fun providesStoreModel(): StoreMoviesDataMVP.Model{
        return StoreModelImp(context)
    }
}