package com.movies.store.presenter

import com.movies.store.StoreMoviesDataMVP
import com.movies.store.model.database.Movie

class StorePresenterImp(var model: StoreMoviesDataMVP.Model) : StoreMoviesDataMVP.Presenter {

    private var view : StoreMoviesDataMVP.View? = null

    init {
        model.setPresenter(this)
    }
    override fun setView(view: StoreMoviesDataMVP.View) {
        this.view = view
    }


    override fun storeMovie(movie: Movie?) {
        if(movie != null) {
            model.storeMovie(movie)
        }else{
            view?.showErrorTryingToSave()
        }
    }

    override fun showInsetedElement() {
        view?.showSavedMovie()
    }

    override fun showInsertingError() {
        view?.showErrorTryingToSave()
    }

    override fun getMoviesStored() {
        model.getMoviesStored()
    }

    override fun showMoviesStored(movies: List<Movie>) {
        view?.showMoviesStored(movies)
    }
}