package com.movies.root

import com.movies.search.SearchModule
import com.movies.store.StoreModule
import com.movies.view.MoviesContainer
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, SearchModule::class, StoreModule::class))
interface ApplicationComponent {
    fun inject(app : App)
    fun inject(searchActivity : MoviesContainer)
}