package com.movies.root

import android.app.Application
import com.movies.search.SearchModule
import com.movies.store.StoreModule

class App : Application() {
    var component : ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .searchModule(SearchModule())
            .storeModule(StoreModule(this))
            .build()

        component!!.inject(this)
    }
}