package com.movies.view.adapteer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.movies.R
import com.movies.store.model.database.Movie
import com.squareup.picasso.Picasso

class MoviesStoredAdapter(var movies : List<Movie>) : RecyclerView.Adapter<MoviesStoredAdapter.MoviesStoredViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesStoredViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.movies_item_list, parent, false)
        return MoviesStoredViewHolder(v)
    }

    override fun onBindViewHolder(holder: MoviesStoredViewHolder, position: Int) {
        val movie = movies.get(position)
        holder.txtTitle.text = movie.movieTitle

        val path = movie.imgagePath
        Picasso.get().load("https://image.tmdb.org/t/p/original" + path)
            .into(holder.ivPoster);

    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class MoviesStoredViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var ivPoster = itemView.findViewById<ImageView>(R.id.ivPoster)
        var txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
    }
}