package com.movies.view.adapteer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.movies.R
import com.movies.search.model.service.response.upcoming.Result
import com.movies.view.listeners.OnItemClickedListener
import com.squareup.picasso.Picasso

class UpcomingMoviesAdapter (private var responseList :List<Result>?, val callback : OnItemClickedListener): RecyclerView.Adapter<UpcomingMoviesAdapter.MoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.movies_item_list, parent, false)
        return MoviesViewHolder(v)
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val movie = responseList?.get(position)
        holder.txtTitle.text = movie?.title

        val path = movie?.posterPath?: "" +R.mipmap.ic_launcher
        Picasso.get().load("https://image.tmdb.org/t/p/original" + path)
            .into(holder.ivPoster);

        holder.crdContainer.setOnClickListener(View.OnClickListener {
            callback.onItemClicked(movie?.id?:0)
        })
    }

    override fun getItemCount(): Int {
        return responseList?.size ?: 0
    }

    fun setList(newList: List<Result>?) {
        responseList = newList
        notifyDataSetChanged()
    }


    class MoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivPoster = itemView.findViewById<ImageView>(R.id.ivPoster)
        var txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
        var crdContainer = itemView.findViewById<CardView>(R.id.crdContainer)
    }
}