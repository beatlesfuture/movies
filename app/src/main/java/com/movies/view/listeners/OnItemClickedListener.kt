package com.movies.view.listeners

interface OnItemClickedListener {
    fun onItemClicked(movieId : Int)
}