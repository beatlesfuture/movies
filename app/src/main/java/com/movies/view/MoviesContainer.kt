package com.movies.view

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.movies.R
import com.movies.search.SearchMoviesMVP
import com.movies.root.App
import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.search.model.service.response.upcoming.Result
import com.movies.store.StoreMoviesDataMVP
import com.movies.store.model.database.Movie
import javax.inject.Inject

class MoviesContainer : AppCompatActivity(), SearchMoviesMVP.View, StoreMoviesDataMVP.View {

    @Inject
    lateinit var presenter : SearchMoviesMVP.Presenter
    @Inject
    lateinit var storePresenter :StoreMoviesDataMVP.Presenter

    lateinit var listFragment : ListMoviesFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movie)
        (application as App).component?.inject(this)

        listFragment = ListMoviesFragment(presenter)
        changeFragment(listFragment)

        storePresenter.getMoviesStored()

    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this, this)
        storePresenter.setView(this)
    }

    private fun changeFragment(currentFragment : Fragment){
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()

        //checando si un fragment ya existe, si es correcto no es necesario volver a crearlo
        if (manager.findFragmentByTag(currentFragment.javaClass.canonicalName) == null) {
            transaction.replace(R.id.frmContainer, currentFragment)
            if(!(currentFragment is ListMoviesFragment)) {
                transaction.addToBackStack("backStack")
            }
            transaction.commitAllowingStateLoss()
        }
    }

    override fun showLatesMovies(latestMovie: LatestMovie) {
        listFragment.showLatesMovie(latestMovie)
    }

    override fun showLatestMovieError(error: String) {
        showToast(error)
    }

    override fun showUpcomingResult(result: List<Result>) {
        listFragment.showUpcomingMovies(result)
    }

    override fun showUpcomingMovieError(error: String) {
        showToast(error)
    }

    override  fun showMovieDetail(movieDetail: MovieDetail?){
        changeFragment(MovieDetailFragment(movieDetail, storePresenter))
    }

    override fun showMovieDetailError(error: String) {
        showToast(error)
    }

    override fun showNoInternetConnection() {
        val builder = AlertDialog.Builder(this)
                .setTitle(getString(R.string.dialog_no_internet_connection_title_))
                .setPositiveButton(getString(R.string.dialog_no_intenet_connection_pos_button_title), DialogInterface.OnClickListener { dialog, id ->
                    listFragment.retryRequestToService()
                })
                .create()

        builder.show()
    }


    fun showToast(msg : String){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showSavedMovie() {
        showToast(getString(R.string.movie_saved_msg))
    }

    override fun showErrorTryingToSave() {
        showToast(getString(R.string.saving_movie_data_error_msg))
    }

    override fun showMoviesStored(movies: List<Movie>) {
        if(movies.size > 0){
            changeFragment(StoredMoviesFragment(movies))
        }

    }

}