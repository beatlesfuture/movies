package com.movies.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.movies.R
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.store.StoreMoviesDataMVP
import com.movies.store.model.database.Movie
import com.squareup.picasso.Picasso

class MovieDetailFragment(val movieDetail: MovieDetail?, val storePresenter : StoreMoviesDataMVP.Presenter) : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.movies_detail_fragment_layout, container, false)

        val ivPosterPath = view.findViewById<ImageView>(R.id.ivPosterPath)
        val txtLanguage = view.findViewById<TextView>(R.id.txtLanguage)
        val txtMovieTitle = view.findViewById<TextView>(R.id.txtMovieTitle)
        val txtOverview = view.findViewById<TextView>(R.id.txtOverview)
        val ivFavourite = view.findViewById<ImageView>(R.id.ivFavourite)



        val path = movieDetail?.posterPath?: "" +R.mipmap.ic_launcher
        Picasso.get().load("https://image.tmdb.org/t/p/original" + path)
                .into(ivPosterPath);

        txtLanguage.text = getString(R.string.language_lable, movieDetail?.originalLanguage)

        txtMovieTitle.setText(movieDetail?.title)

        txtOverview.text = movieDetail?.overview

        ivFavourite.setOnClickListener(View.OnClickListener {
            storePresenter.storeMovie(
                Movie(
                    movieDetail?.id!!,
                    movieDetail?.title,
                    (movieDetail?.posterPath?: "") as String,
                    movieDetail?.overview,
                    movieDetail?.originalLanguage)
            )
        })

        return view
    }
}