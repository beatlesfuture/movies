package com.movies.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.R
import com.movies.search.SearchMoviesMVP
import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.upcoming.Result
import com.movies.store.StoreMoviesDataMVP
import com.movies.view.adapteer.MoviesAdapter
import com.movies.view.adapteer.UpcomingMoviesAdapter
import com.movies.view.listeners.OnItemClickedListener

class ListMoviesFragment(val presenter: SearchMoviesMVP.Presenter) : Fragment(), RadioGroup.OnCheckedChangeListener,
OnItemClickedListener{


    private var rgType : RadioGroup? = null
    private var rvMovies : RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.list_movies_fragment_layout, container, false)

        rgType = view.findViewById(R.id.rgType)
        rvMovies = view.findViewById(R.id.rvMovies)


        rgType?.setOnCheckedChangeListener(this)

        rvMovies?.layoutManager = LinearLayoutManager(activity)
        rvMovies?.isMotionEventSplittingEnabled = false
        rvMovies?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))


        return view
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
        when(p1){
            R.id.rbLatest->
                presenter.searchLastesMovies()
            R.id.rbUpcoming->
                presenter.searchUpcomingMovies()
            R.id.rbPopular->
                presenter.searchPopularMovies()

        }
    }

    fun showLatesMovie(latestMovie : LatestMovie){
        val list : List<LatestMovie> = listOf(latestMovie)
        rvMovies?.adapter = MoviesAdapter(list, this)
    }

    fun showUpcomingMovies(upcomingMovies : List<Result>){
        rvMovies?.adapter = UpcomingMoviesAdapter(upcomingMovies, this)
    }

    override fun onItemClicked(movieId: Int) {
        presenter.getMovieDetail(movieId)
    }

    fun retryRequestToService(){
        var rbChecked = rgType?.checkedRadioButtonId ?: -1
        onCheckedChanged(rgType, rbChecked)
    }
}