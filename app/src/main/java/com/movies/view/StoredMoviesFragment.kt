package com.movies.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.R
import com.movies.store.model.database.Movie
import com.movies.view.adapteer.MoviesStoredAdapter

class StoredMoviesFragment(var movies : List<Movie>) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.list_stored_movies_layout, container, false)

        val rvMoviesStored = view.findViewById<RecyclerView>(R.id.rvMoviesStored)
        rvMoviesStored?.layoutManager = LinearLayoutManager(activity)
        rvMoviesStored?.isMotionEventSplittingEnabled = false
        rvMoviesStored?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))

        rvMoviesStored.adapter = MoviesStoredAdapter(movies)

        return view
    }

}