package com.movies.search

import com.movies.search.model.SearchMoviesModelImp
import com.movies.search.presenter.SearchMoviesPresenterImp
import dagger.Module
import dagger.Provides

@Module
class SearchModule {
    @Provides
    fun providesPresenter(model: SearchMoviesMVP.Model) : SearchMoviesMVP.Presenter{
        return SearchMoviesPresenterImp(model)
    }
    @Provides
    fun providesModel(): SearchMoviesMVP.Model{
        return SearchMoviesModelImp()
    }
}