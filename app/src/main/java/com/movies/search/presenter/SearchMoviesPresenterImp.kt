package com.movies.search.presenter

import android.content.Context
import android.net.ConnectivityManager
import com.movies.search.SearchMoviesMVP
import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.search.model.service.response.upcoming.Result

class SearchMoviesPresenterImp (var model : SearchMoviesMVP.Model) : SearchMoviesMVP.Presenter {
    private var view : SearchMoviesMVP.View? = null
    private var context : Context? = null

    init {
        model.setPrersenter(this)
    }
    override fun setView(view: SearchMoviesMVP.View, context: Context) {
        this.view = view
        this.context = context
    }

    //Latest Movies
    override fun searchLastesMovies() {
        if (!inNetworkAvailable()){
            view!!.showNoInternetConnection()
            return
        }
        model.searchLatesMovies()
    }

    override fun showLatestMoviesResult(latestMovie: LatestMovie) {
        view?.showLatesMovies(latestMovie)
    }

    override fun showLatestMovieError(error: String) {
       view?.showLatestMovieError(error)
    }


    //Upcoming movies
    override fun searchUpcomingMovies() {
        if (!inNetworkAvailable()){
            view!!.showNoInternetConnection()
            return
        }
        model.searchUpcomingMovies()
    }

    override fun showUpcomingResult(result: List<Result>) {
        view?.showUpcomingResult(result)
    }

    override fun showUpcomingMovieError(error: String) {
        view?.showUpcomingMovieError(error)
    }

    //Movies Detail
    override fun getMovieDetail(id: Int) {
        if (!inNetworkAvailable()){
            view!!.showNoInternetConnection()
            return
        }
        model.getMovieDetail(id)
    }

    override fun showMovieDetail(movieDetail: MovieDetail) {
        view?.showMovieDetail(movieDetail)
    }

    override fun showMovieDetailError(error: String) {
        view?.showMovieDetailError(error)
    }

    //Popular movies
    override fun searchPopularMovies() {
        if (!inNetworkAvailable()){
            view!!.showNoInternetConnection()
            return
        }
       model.searchPopularMovies()
    }

    fun inNetworkAvailable() : Boolean{
        val manager  = (this.context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        val networkInfo = manager.getActiveNetworkInfo()
        if(networkInfo != null) {
            when (networkInfo.type){
                ConnectivityManager.TYPE_MOBILE ->
                    return true
                ConnectivityManager.TYPE_WIFI ->
                    return true
                else -> return false
            }
        }
        return false
    }
}