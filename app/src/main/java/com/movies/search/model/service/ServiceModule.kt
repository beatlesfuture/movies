package com.movies.search.model.service

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ServiceModule {

    companion object {
        val API_KEY = "411a714eb512db9d48f8579e617df2d6"
    }
    private val BASE_URL = "https://api.themoviedb.org/"

    @Provides
    fun providesRetorfit() : Retrofit{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun providesService() : ApiService {
        return  providesRetorfit().create(ApiService::class.java)
    }
}
