package com.movies.search.model

import com.movies.search.SearchMoviesMVP
import com.movies.search.model.service.ApiService
import com.movies.search.model.service.DaggerAppiServiceComponent
import com.movies.search.model.service.ServiceModule
import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.search.model.service.response.upcoming.Movie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class SearchMoviesModelImp : SearchMoviesMVP.Model {
    init {
        DaggerAppiServiceComponent.builder()
            .serviceModule(ServiceModule())
            .build()
            .inject(this)
    }

    @Inject
    lateinit var service : ApiService
    lateinit var presenter: SearchMoviesMVP.Presenter

    override fun setPrersenter(presenter: SearchMoviesMVP.Presenter) {
        this.presenter = presenter
    }

    override fun searchLatesMovies() {
        CoroutineScope(Dispatchers.IO).launch {
            val call = service.searchLatestMovies(ServiceModule.API_KEY)

            call.enqueue(object : Callback<LatestMovie> {
                override fun onFailure(call: Call<LatestMovie>, t: Throwable) {
                    presenter!!.showLatestMovieError(t.message?:"")
                }

                override fun onResponse(call: Call<LatestMovie>, serviceResponse: Response<LatestMovie>) {
                    if(serviceResponse.errorBody() == null) {
                        presenter.showLatestMoviesResult(serviceResponse.body()!!)
                    }else{
                        presenter.showLatestMovieError(serviceResponse.message())
                    }
                }

            })
        }
    }

    override fun searchUpcomingMovies() {
        CoroutineScope(Dispatchers.IO).launch {
            val call = service.searchUpcomingMovies(ServiceModule.API_KEY)

            call.enqueue(object : Callback<Movie> {
                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    presenter.showUpcomingMovieError(t.message?:"")
                }

                override fun onResponse(call: Call<Movie>, serviceResponse: Response<Movie>) {
                    if(serviceResponse.errorBody() == null) {
                        presenter.showUpcomingResult(serviceResponse.body()?.results!!)
                    }else{
                        presenter.showUpcomingMovieError(serviceResponse.message())
                    }
                }

            })
        }
    }

    override fun searchPopularMovies() {
        CoroutineScope(Dispatchers.IO).launch {
            val call = service.searchPopularMovies(ServiceModule.API_KEY)

            call.enqueue(object : Callback<Movie> {
                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    presenter.showUpcomingMovieError(t.message?:"")
                }

                override fun onResponse(call: Call<Movie>, serviceResponse: Response<Movie>) {
                    if(serviceResponse.errorBody() == null) {
                        presenter.showUpcomingResult(serviceResponse.body()?.results!!)
                    }else{
                        presenter.showUpcomingMovieError(serviceResponse.message())
                    }
                }

            })
        }
    }

    override fun getMovieDetail(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val call = service.searchMovieDetail(id, ServiceModule.API_KEY)

            call.enqueue(object : Callback<MovieDetail> {
                override fun onFailure(call: Call<MovieDetail>, t: Throwable) {
                    presenter.showMovieDetailError(t.message?:"")
                }

                override fun onResponse(call: Call<MovieDetail>, serviceResponse: Response<MovieDetail>) {
                    if(serviceResponse.errorBody() == null) {

                        presenter.showMovieDetail(serviceResponse.body()!!)
                    }else{
                        presenter.showMovieDetailError(serviceResponse.message())
                    }
                }

            })
        }
    }
}