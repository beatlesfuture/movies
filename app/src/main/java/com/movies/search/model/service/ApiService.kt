package com.movies.search.model.service

import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.search.model.service.response.upcoming.Movie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("3/movie/latest?")
    fun searchLatestMovies(@Query("api_key") key : String) : Call<LatestMovie>

    @GET("3/movie/upcoming?")
    fun searchUpcomingMovies(@Query("api_key") key : String) : Call<Movie>

    @GET("3/movie/popular?")
    fun searchPopularMovies(@Query("api_key") key : String) : Call<Movie>

    @GET("3/movie/{movie_id}")
    fun searchMovieDetail( @Path("movie_id") movieId : Int, @Query("api_key") key : String) : Call<MovieDetail>
}