package com.movies.search.model.service

import com.movies.search.model.SearchMoviesModelImp
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf( ServiceModule::class))
interface AppiServiceComponent {
    fun inject(modelImp : SearchMoviesModelImp)
}