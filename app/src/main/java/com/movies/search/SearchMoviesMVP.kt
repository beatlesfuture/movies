package com.movies.search

import android.content.Context
import com.movies.search.model.service.response.LatestMovie
import com.movies.search.model.service.response.detail.MovieDetail
import com.movies.search.model.service.response.upcoming.Result

interface SearchMoviesMVP {
    interface View{
        fun showLatesMovies(latestMovie: LatestMovie)
        fun showLatestMovieError(error: String)

        fun showUpcomingResult(result: List<Result>)
        fun showUpcomingMovieError(error: String)

        fun showMovieDetail(movieDetail: MovieDetail?)
        fun showMovieDetailError(error : String)

        fun showNoInternetConnection()
    }

    interface Presenter{
        fun setView(view : View, context: Context)

        fun searchLastesMovies()
        fun searchUpcomingMovies()
        fun searchPopularMovies()

        //showing Latest result
        fun showLatestMoviesResult(latestMovie : LatestMovie)
        fun showLatestMovieError(error : String)

        //showwing upcomming result
        fun showUpcomingResult(result: List<Result>)
        fun showUpcomingMovieError(error: String)


        //getting movie detail
        fun getMovieDetail(id : Int)
        fun showMovieDetail(movieDetail: MovieDetail)
        fun showMovieDetailError(error : String)
    }

    interface Model{
        fun setPrersenter(presenter: Presenter)

        fun searchLatesMovies()
        fun searchUpcomingMovies()
        fun searchPopularMovies()

        fun getMovieDetail(id : Int)
    }
}